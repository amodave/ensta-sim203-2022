// Description:
//   Trapezoidal rule with 'pragma omp parallel for'
// Compilation:
//   icc -std=c99 -qopenmp trapeze8.c
// Execution:
//   export OMP_NUM_THREADS=4
//   ./a.out

#include <omp.h>
#include <math.h>
#include <stdio.h>

static inline double f(double x){
  return sin(x);
}

int main(){

  double a = 0.;
  double b = M_PI;
  int    N = 1e6;

  double h = (b-a)/N;
  double approx = (f(a) + f(b))*0.5;

#pragma omp parallel for reduction(+:approx)
  for(int i=1; i<=N-1; i++){
    double x_i = a + i*h;
    approx += f(x_i);
  }

  approx *= h;

  printf("Result: %f\n", approx);
  return 0;
}
