// Description:
//   Trapezoidal rule in a serial code
// Compilation:
//   icc -std=c99 trapeze1.c
// Execution:
//   ./a.out

#include <math.h>
#include <stdio.h>

static inline double f(double x){
  return sin(x);
}

double trapeze(double a, double b, int N){
  double h = (b-a)/N;
  double approx = (f(a) + f(b))*0.5;
  for(int i=1; i<=N-1; i++){
    double x_i = a + i*h;
    approx += f(x_i);
  }
  approx *= h;
  return approx;
}

int main(){

  // Parameters
  double a = 0.;
  double b = M_PI;
  int    N = 1000;

  // Numerical integration
  double approx = trapeze(a, b, N);

  // Print result
  printf("Result: %f\n", approx);

  return 0;
}

