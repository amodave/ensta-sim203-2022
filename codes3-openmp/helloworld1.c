// Compilation:
//   icc -qopenmp helloworld1.c
// Execution:
//   ./a.out

#include <stdio.h>

int main(){

  int N;
  printf("How many threads? ");
  scanf("%i", &N);  

#pragma omp parallel num_threads(N)
  printf("Hello world!\n");

  return 0;
}
