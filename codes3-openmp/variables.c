// Compilation:
//   icc -qopenmp variables.c
// Execution:
//   export OMP_NUM_THREADS=4
//   ./a.out

#include <stdio.h>
#include <omp.h>

int main(){

  int A = 10;
  int B = 20;
  int C = 30;

  printf("S: %i %i %i\n", A, B, C);

  #pragma omp parallel default(none) shared(A) \
    private(B) firstprivate(C)
  {
    A += omp_get_thread_num();
    B += omp_get_thread_num();
    C += omp_get_thread_num();
    printf("P: %i %i %i\n", A, B, C);
  }

  printf("S: %i %i %i\n", A, B, C);

  return 0;
}
