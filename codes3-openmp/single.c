// Compilation:
//   icc -qopenmp single.c
// Execution:
//   export OMP_NUM_THREADS=4
//   ./a.out

#include <omp.h>
#include <stdio.h>

int main(){

#pragma omp parallel
  {

    double a = 92290.;

#pragma omp single
    {
      a = -92290.;
    }

    int myRank = omp_get_thread_num();
    printf("Rank %i: %f\n", myRank, a);
  }

  return 0;
}
