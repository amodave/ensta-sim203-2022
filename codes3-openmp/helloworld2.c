// Compilation:
//   icc -qopenmp helloworld2.c
// Execution:
//   ./a.out

#include <stdio.h>
#include <omp.h>

int main(){

  int N;
  printf("How many threads? ");
  scanf("%i", &N);
  omp_set_num_threads(N);

#pragma omp parallel
  printf("Hello world!\n");

  return 0;
}
