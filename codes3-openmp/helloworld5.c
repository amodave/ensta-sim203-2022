// Compilation:
//   icc -qopenmp helloworld4.c
// Execution:
//   export OMP_NUM_THREADS=4
//   ./a.out

#include <stdio.h>
#include <omp.h>

void myPrint(char message[]){
  int myRank     = omp_get_thread_num();
  int numThreads = omp_get_num_threads();
  printf("%s (%i/%i)\n", message, myRank, numThreads);
}

int main(){

  printf("Let's start!\n");

#pragma omp parallel
  myPrint("Hello world!");

  myPrint("I'm done!");

  return 0;
}

