// Compilation:
//   icc -qopenmp sections.c
// Execution:
//   export OMP_NUM_THREADS=4
//   ./a.out

#include <omp.h>
#include <stdio.h>

int main(){

#pragma omp parallel
  {
    int n = omp_get_thread_num();
    printf("%i (I am alive!)\n", n);

#pragma omp sections
    {
#pragma omp section
      printf("%i (I am the first!)\n", n);
#pragma omp section
      {
        printf("%i (I am the second!)\n", n);
      }
    }
  }

  return 0;
}
