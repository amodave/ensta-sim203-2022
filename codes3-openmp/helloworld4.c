// Compilation:
//   icc -qopenmp helloworld4.c
// Execution:
//   export OMP_NUM_THREADS=4
//   ./a.out

#include <stdio.h>
#include <omp.h>

int main(){

  printf("Let's start!\n");

  #pragma omp parallel
  {
    int myRank     = omp_get_thread_num();
    int numThreads = omp_get_num_threads();
    printf("Hello world! (%i/%i)\n", myRank, numThreads);
  }

  int myRank     = omp_get_thread_num();
  int numThreads = omp_get_num_threads();
  printf("I'm done! (%i/%i)\n", myRank, numThreads);

  return 0;
}

