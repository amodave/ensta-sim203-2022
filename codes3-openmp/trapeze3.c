// Description:
//   Trapezoidal rule with clause for shared/private variables
// Compilation:
//   icc -std=c99 -qopenmp trapeze3.c
// Execution:
//   export OMP_NUM_THREADS=4
//   ./a.out

#include <omp.h>
#include <math.h>
#include <stdio.h>

static inline double f(double x){
  return sin(x);
}

double trapeze(double a, double b, int N){
  double h = (b-a)/N;
  double approx = (f(a) + f(b))*0.5;
  for(int i=1; i<=N-1; i++){
    double x_i = a + i*h;
    approx += f(x_i);
  }
  approx *= h;
  return approx;
}

int main(){

  double a = 0.;
  double b = M_PI;
  int    N = 1000;

  double my_a;
  double my_b;
  int    my_N;
  double approx;

  #pragma omp parallel shared(a,b,N) \
    private(my_a,my_b,my_N,approx)
  {
    int myRank     = omp_get_thread_num();
    int numThreads = omp_get_num_threads();

    my_a = a + (b-a)*myRank/numThreads;
    my_b = a + (b-a)*(myRank+1)/numThreads;
    my_N = N/numThreads;
    approx = trapeze(my_a, my_b, my_N);

    printf("Result: %f\n", approx);
  }

  return 0;
}
