// Compilation:
//   icc -qopenmp helloworld3.c
// Execution:
//   export OMP_NUM_THREADS=4
//   ./a.out

#include <stdio.h>

int main(){

#pragma omp parallel
  printf("Hello world!\n");

  return 0;
}
