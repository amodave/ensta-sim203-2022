// Compilation
//   icc -std=c99 -mkl dgemvMKL.c 
// Execution:
//   ./a.out

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <mkl.h>

#ifndef M_PI
  #define M_PI 3.14159265359
#endif

void initArray(double *array, int size) {
  for(int i=0; i<size; i++)
    array[i] = sin(M_PI*(1./((double)i+1)));
}

int main()
{

for(int iter=1; iter<=100; iter++){

  // parameter of the problem
  int n = iter*200;
  int LOOP = 200;
  if(n<=2500) LOOP = 10000;
  if(n<=5000) LOOP = 1000;
  else if(n<=10000) LOOP = 100;
  else if(n<=15000) LOOP = 50;
  else LOOP = 20;

  // parameters for BLAS
  enum CBLAS_LAYOUT Layout = CblasRowMajor;
  enum CBLAS_TRANSPOSE TransA = CblasNoTrans;
  int M = n;
  int N = n;
  int K = n;
  int lda = n;
  int incx = 1;
  int incy = 1;

  // scalar and matrices
  double alpha = 1.;
  double beta  = 1.;
  double *A = malloc(n*n * sizeof(double));
  double *x = malloc(n   * sizeof(double));
  double *y = malloc(n   * sizeof(double));
  initArray(A, n*n);
  initArray(x, n);
  initArray(y, n);

  // timing: start
  const clock_t timeBegin1 = clock();
  const double timeBegin2 = dsecnd();
  const double FLOP = 2*n*(n+1);

  for(int i=0; i<LOOP; i++){
    cblas_dgemv(Layout, TransA, M, N, alpha, A, lda, x, incx, beta, y, incy);
  }

  // timing: end
  const clock_t timeEnd1 = clock();
  const double timeEnd2 = dsecnd();

  double timeTotal1 = (double)(timeEnd1-timeBegin1)/LOOP/CLOCKS_PER_SEC;
  double timeTotal2 = (double)(timeEnd2-timeBegin2)/LOOP;
//  printf("%f %f %g %g\n", timeTotal1, timeTotal2, FLOP/timeTotal1, FLOP/timeTotal2);
  printf("%i %f %g\n", n, timeTotal2, FLOP/timeTotal2);

  free(A);
  free(x);
  free(y);

}
  return 0;
}
