// Compilation:
//   icc -O0 -std=c99 -mkl diffusionIcc.c
//   icc -O1 -std=c99 -mkl diffusionIcc.c 
//   icc -O2 -std=c99 -mkl diffusionIcc.c
//   icc -O3 -std=c99 -mkl diffusionIcc.c
//   icc -O3 -std=c99 -mkl -qopt-report=1 -qopt-report-annotate=html diffusionIcc.c
// Execution:
//   ./a.out 'version' 'T' 'N'
//   ./a.out 1 1000 1024; ./a.out 2 1000 1024; ./a.out 3 1000 1024; ./a.out 4 1000 1024;

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mkl.h>

void switchPointer(double** x1, double** x2);
void implementation1(int T, int N, double* C, double* Cnew);
void implementation2(int T, int N, double* C, double* Cnew);
void implementation3(int T, int N, double* C, double* Cnew);

int main(int argc, char* argv[])
{

  // Parameters
  int version, T, N;
  if(argc == 4){
    version = atoi(argv[1]);
    T = atoi(argv[2]);
    N = atoi(argv[3]);
  }
  else{
    printf("3 arguments nécessaires : version, T, N\n");
    return 1;
  }

  // Initialization of arrays
  double* C    = malloc(N*N*sizeof(double));
  double* Cnew = malloc(N*N*sizeof(double));
  for(int i=0; i<N; i++){
    for(int j=0; j<N; j++){
      double x = ((double)i)/(N-1.);
      double y = ((double)j)/(N-1.);
      C[i*N+j] = exp(-((x-0.5)*(x-0.5)+(y-0.5)*(y-0.5))/0.01);
    }
  }

  // Start CHRONO
  const double timeBegin = dsecnd();

  if(version == 1) implementation1(T, N, C, Cnew);
  if(version == 2) implementation2(T, N, C, Cnew);
  if(version == 3) implementation3(T, N, C, Cnew);

  // End CHRONO
  const double timeEnd = dsecnd();
  double timeTotal = timeEnd-timeBegin;
  printf("%i %f %f\n", version, timeTotal, C[N*N/2 + N/2]);

  return 0;
}

// Switch of pointers
void switchPointer(double** x1, double** x2){
  double* tmp;
  tmp = *x1;
  *x1 = *x2;
  *x2 = tmp;
}

// Implementation 1
void implementation1(int T, int N, double* C, double* Cnew){
  double dx = 1./(double)(N-1);
  double dt = 0.2*dx*dx;

  for(int n=0; n<T; n++){
    for(int j=1; j<(N-1); j++)
      for(int i=1; i<(N-1); i++)
        Cnew[N*i+j]
          = (1 - 4*dt/(dx*dx)) * C[N*i+j]
          + dt/(dx*dx) * ( C[N*(i+1)+j] + C[N*(i-1)+j] + C[N*i+(j+1)] + C[N*i+(j-1)] );
    switchPointer(&C,&Cnew);
  }
}

// Implementation 2
void implementation2(int T, int N, double* C, double* Cnew){
  double dx = 1./(double)(N-1);
  double dt = 0.2*dx*dx;

  for(int n=0; n<T; n++){
    for(int i=1; i<(N-1); i++)
      for(int j=1; j<(N-1); j++)
        Cnew[N*i+j]
          = (1 - 4*dt/(dx*dx)) * C[N*i+j]
          + dt/(dx*dx) * ( C[N*(i+1)+j] + C[N*(i-1)+j] + C[N*i+(j+1)] + C[N*i+(j-1)] );
    switchPointer(&C,&Cnew);
  }
}

// Implementation 3
void implementation3(int T, int N, double* C, double* Cnew){
  double dx = 1./(double)(N-1);
  double dt = 0.2*dx*dx;
  double coef1 = dt/(dx*dx);
  double coef2 = 1 - 4*coef1;

  for(int n=0; n<T; n++){
    for(int i=1; i<(N-1); i++)
      for(int j=1; j<(N-1); j++)
        Cnew[N*i+j]
          = coef2 * C[N*i+j]
          + coef1 * ( C[N*(i+1)+j] + C[N*(i-1)+j] + C[N*i+(j+1)] + C[N*i+(j-1)] );
    switchPointer(&C,&Cnew);
  }
}
