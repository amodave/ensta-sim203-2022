// Compilation
//   icc -std=c99 -mkl dgemmMKL.c 
// Execution:
//   ./a.out

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <mkl.h>

#ifndef M_PI
  #define M_PI 3.14159265359
#endif

void initArray(double *array, int size) {
  for(int i=0; i<size; i++)
    array[i] = sin(M_PI*(1./((double)i+1)));
}

int main()
{

for(int iter=100; iter<=100; iter++){

  // parameter of the problem
  int n = iter*10;
  int LOOP = 200;
  if(n<=250) LOOP = 2000;
  else if(n<=500) LOOP = 1000;
  else if(n<=750) LOOP = 500;
  else LOOP = 200;
  
  // parameters for BLAS
  enum CBLAS_LAYOUT Layout = CblasRowMajor;
  enum CBLAS_TRANSPOSE TransA = CblasTrans;
  enum CBLAS_TRANSPOSE TransB = CblasNoTrans;
  int M = n;
  int N = n;
  int K = n;
  int lda = n;
  int ldb = n;
  int ldc = n;

  // scalar and matrices
  double alpha = 1.;
  double beta  = 1.;
  double *A = malloc(n*n * sizeof(double));
  double *B = malloc(n*n * sizeof(double));
  double *C = malloc(n*n * sizeof(double));
  initArray(A, n*n);
  initArray(B, n*n);
  initArray(C, n*n);

  // timing: start
  const clock_t timeBegin1 = clock();
  const double timeBegin2 = dsecnd();
  const double FLOP = 2*n*n*(n+1);

  for(int i=0; i<LOOP; i++){
    cblas_dgemm(Layout, TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc);
  }

  // timing: end
  const clock_t timeEnd1 = clock();
  const double timeEnd2 = dsecnd();

  double timeTotal1 = (double)(timeEnd1-timeBegin1)/LOOP/CLOCKS_PER_SEC;
  double timeTotal2 = (double)(timeEnd2-timeBegin2)/LOOP;
//  printf("%f %f %g %g\n", timeTotal1, timeTotal2, FLOP/timeTotal1, FLOP/timeTotal2);
  printf("%i %f %g\n", n, timeTotal2, FLOP/timeTotal2);

  free(A);
  free(B);
  free(C);

}
  return 0;
}
