// Compilation
//   icc -std=c99 -mkl sgemmMKL.c 
// Execution:
//   ./a.out

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <mkl.h>

#ifndef M_PI
  #define M_PI 3.14159265359
#endif

void initArray(float *array, int size) {
  for(int i=0; i<size; i++)
    array[i] = sin(M_PI*(1./((double)i+1)));
}

int main()
{

for(int iter=1; iter<=100; iter++){

  // parameter of the problem
  int n = iter*10;
  int LOOP = 200;
  if(n<=250) LOOP = 2000;
  else if(n<=500) LOOP = 1000;
  else if(n<=750) LOOP = 500;
  else LOOP = 200;

  // parameters for BLAS
  enum CBLAS_LAYOUT Layout = CblasRowMajor;
  enum CBLAS_TRANSPOSE TransA = CblasTrans;
  enum CBLAS_TRANSPOSE TransB = CblasNoTrans;
  int M = n;
  int N = n;
  int K = n;
  int lda = n;
  int ldb = n;
  int ldc = n;

  // scalar and matrices
  float alpha = 1.;
  float beta  = 1.;
  float *A = malloc(n*n * sizeof(float));
  float *B = malloc(n*n * sizeof(float));
  float *C = malloc(n*n * sizeof(float));
  initArray(A, n*n);
  initArray(B, n*n);
  initArray(C, n*n);

  // timing: start
  const double timeBegin = dsecnd();
  const double FLOP = 2*n*n*(n+1);

  for(int i=0; i<LOOP; i++){
    cblas_sgemm(Layout, TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc);
  }

  // timing: end
  const double timeEnd = dsecnd();
  double timeTotal = (double)(timeEnd-timeBegin)/LOOP;
  printf("%i %f %g\n", n, timeTotal, FLOP/timeTotal);

  free(A);
  free(B);
  free(C);

}
  return 0;
}
