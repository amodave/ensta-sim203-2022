#ifndef TOOLS_HEADER
#define TOOLS_HEADER

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mkl.h>

#define NFacesLin    2
#define NFacesTri    3
#define NFacesTet    4
#define NFaceVertLin 1
#define NFaceVertTri 2
#define NFaceVertTet 3
#define NVertLin     2
#define NVertTri     3
#define NVertTet     4

// DGESV - from LAPACK library
void dgesv_(const int *N, const int *NRHS, double *A, const int *LDA, int *IPIV, double *B, const int *LDB, int *INFO );

// Compute factorial of number 'n'
int factorial(int n);

// Compute inverseMat of n-matrix A (store the inverseMat in B)
void inverseMat(int N, double *A, double *B);

// Compute Jacobi polynomial
double jacobiP(double xout, double alpha, double beta, int p);

// Compute derivative of Jacobi polynomial
double gradJacobiP(double xout, double alpha, double beta, int p);

// Compute derivatives of the modal basis (id,jd,kd) on the simplex at (a,b,c).
void gradSimplexP(double a_out, double b_out, double c_out, int id, int jd, int kd, double *dmodedr, double *dmodeds, double *dmodedt);

// Compute 2D Vandermonde matrix
void vandermonde2D(int N, int Nfp, double *r, double *s, double *Vout);

// Compute 3D Vandermonde matrix
void vandermonde3D(int N, int Np, double *r, double *s, double *t, double *Vout);

// Compute gradient of 3D Vandermonde matrix
void gradVandermonde3D(int N, int Np, double *r, double *s, double *t, double *Vrout, double *Vsout, double *Vtout);

#endif
