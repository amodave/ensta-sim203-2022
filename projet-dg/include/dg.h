#ifndef DG_HEADER
#define DG_HEADER

#include "tools.h"

// Build connectivity arrays
void buildEToE(int K, int *EToV, int **_EToE, int **_EToF);
int compareEntries(const void *a, const void *b);

// Build connectivity face nodes (mapping faceNode-to-faceNode)
void buildNfToNf(int K, int Nfp, int Np, int *_Fmask, int *_EToE, int *_EToF, double *_x, double *_y, double *_z, double _TOL, int **_mapP);

// Build geometric factors
void buildGeomFactors(int K, int *_EToV, double *_VX, double *_VY, double *_VZ, double **_rstxyz, double **_Fscale, double **_nx, double **_ny, double **_nz);

// Build elemental matrices
void buildElemMatrices(int N, int Np, int Nfp, double *_r, double *_s, double *_t, int *_Fmask, double **_Dr, double **_Ds, double **_Dt, double **_LIFT);

#endif
