#ifndef GMSH_HEADER
#define GMSH_HEADER

#include "tools.h"

// Load mesh from gmsh file
void loadMeshGmsh(char* fileName, int *K, double **_VX, double **_VY, double **_VZ, int **_ETag, int **_EMsh, int **_EToV);

// Export parameters in gmsh files
void exportParamGmsh(double *_data, int K, int Nparams, int *_EMsh);

// Export discrete solution in gmsh files
void exportSolGmsh(double *_data, int K, int N, int Np, int Nfields, double *_r, double *_s, double *_t, int *_EMsh, int timeStep, double time);

#endif
