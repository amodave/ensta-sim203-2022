# ENSTA Paris - SIM203 - Projet _"Éléments finis discontinus"_

Consignes : [https://perso.ensta-paris.fr/~modave/SIM203/](https://perso.ensta-paris.fr/~modave/SIM203/)

## Objectifs pour le premier TD

* Utiliser les commandes expliquées dans ce fichier README
* Comprendre les grandes étapes du fichier `main.c` (avec les numéros 1 à 8)
* Faire le lien entre les formules du cours théorique et l’étape numéro 7 du fichier `main.c`
* Comprendre la signification des paramètres du fichier `setup`

## Commandes de compilation

1. Avant la compilation, créez un dossier qui contiendra les fichiers de compilation : `mkdir obj`
2. Compiler le programme avec la commande suivante : `make`
La compilation crée l’exécutable 'dg' dans le répertoire principal. Une re-compilation avec `make` ne re-compilera que les fichiers modifiés.
3. Pour effacer les fichiers de compilation et l’exécutable : `make clean`

## Exécution d’un benchmark

1. Une fois le programme compilé, allez dans un dossier de benchmark : `cd benchmark/cube`. Ce dossier contient une géométrie au format ‘gmsh’ (mesh.geo), un fichier de configuration de benchmark pour le programme ‘dg’ (setup) et un script ‘gmsh’ qui peut être utiliser pour visualiser la solution (visu.script).
2. Avant l’exécution, créez un dossier qui contiendra les ‘outputs’ du programme : ``mkdir output``
Et générez le maillage : ``gmsh mesh.geo -3``
3. Exécutez le programme à partir du dossier du benchmark : `../../dg`
Lisez ce qui s’affiche, et regardez le contenu du dossier ‘output’ à la fin de l’exécution.
Attention, l’exécution doit se faire dans le dossier du benchmark !
Le programme ‘dg’ lit automatiquement le maillage ‘mesh.msh’ et le fichier ‘setup’.

## Visualisation des résultats

1. A la fin de l’exécution, on peut visualiser la solution avec le logiciel *gmsh* : `gmsh mesh.msh output/*`
2. Pour une visualisation plus jolie, on peut utiliser le script : `gmsh vizu.script`
