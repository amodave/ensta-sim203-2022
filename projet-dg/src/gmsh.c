#include "gmsh.h"

// ======================================================================
// Load mesh from gmsh file
//   Input: 'fileName', NVertTet
//   Output: K, _VX, _VY, _VZ, _ETag, _EMsh, _EToV
// ======================================================================

void loadMeshGmsh(char *fileName, int *_K, double **_VX, double **_VY, double **_VZ, int **_ETag, int **_EMsh, int **_EToV){
  printf("CALL loadMeshGmsh('%s')\n", fileName);
  
  FILE* meshfile = NULL;
  meshfile = fopen(fileName, "r");
  assert(meshfile != NULL);
  
  char line[128] = "";
  int allVertices, allElements, dummy;
  while(fgets(line, 128, meshfile) != NULL){
    
    // scan vertices in gmsh file
    if(strstr(line, "$Nodes") != NULL){
      
      // read number of vertices
      fscanf(meshfile, "%i\n", &allVertices);
      
      // read line of each vertex, ignoring gmsh index of vertex
      double *VX = malloc(allVertices*sizeof(double));
      double *VY = malloc(allVertices*sizeof(double));
      double *VZ = malloc(allVertices*sizeof(double));
      for(int v=0; v<allVertices; ++v){
        fscanf(meshfile, "%i %lf %lf %lf", &dummy, &VX[v], &VY[v], &VZ[v]);
      }
      
      // save arrays
      *_VX = VX;
      *_VY = VY;
      *_VZ = VZ;
    }
    
    // scan elements in gmsh file
    if(strstr(line, "$Elements") != NULL){
      
      // read number of elements
      fscanf(meshfile, "%i\n", &allElements);
      
      // record location in file
      fpos_t position;
      fgetpos(meshfile, &position);
      
      // count number of elements per type
      int K=0;
      int eGmshNum, eGmshType, eTag;
      for(int k=0; k<allElements; k++) {
        fscanf(meshfile, "%i %i %i %i %i", &eGmshNum, &eGmshType, &dummy, &eTag, &dummy);
        if(eGmshType==2)
          fscanf(meshfile, "%i %i %i", &dummy, &dummy, &dummy);
        if(eGmshType==4) {
          fscanf(meshfile, "%i %i %i %i", &dummy, &dummy, &dummy, &dummy);
          K++;
        }
      }
      assert(K>0);
      
      // rewind to beginning of element list
      fsetpos(meshfile, &position);
      
      // build EToV, ETag, EMsh for type of element
      int kLoc = 0;
      int *EToV = malloc(K*NVertTet*sizeof(int));
      int *ETag = malloc(K*sizeof(int));
      int *EMsh = malloc(K*sizeof(int));
      for(int kGlo=0; kGlo<allElements; kGlo++) {
        
        fscanf(meshfile, "%i %i %i %i %i", &eGmshNum, &eGmshType, &dummy, &eTag, &dummy);
        if(eGmshType==2)
          fscanf(meshfile, "%i %i %i", &dummy, &dummy, &dummy);
        if(eGmshType==4){   // 4 for TET
          int k = kLoc;
          EMsh[k] = eGmshNum; // save the gmsh number
          ETag[k] = eTag;     // save the element group (physical gmsh label)
          for(int v=0; v<NVertTet; ++v){
            int vertGmsh;
            fscanf(meshfile, "%i", &vertGmsh);   // save the associated vertices
            EToV[k*NVertTet + v] = vertGmsh-1;  // (gmsh is 1-index, here is 0-index)
          }
          kLoc++;
        }
      }
      
      // save arrays
      *_K = K;
      *_EToV = EToV;
      *_ETag = ETag;
      *_EMsh = EMsh;
    }
  }
  
  fclose(meshfile);
  
  printf("INFO: Load %i vertices and %i tetrahedral elements from GMSH file (%s)\n", allVertices, *_K, fileName);
}


// ======================================================================
// Export parameters in gmsh files
//   Input: K, NVertTet, _EMsh, fileName, viewName, Q
//   Output: -
// ======================================================================

void exportParamGmsh(double *data, int K, int Nparams, int *EMsh){
  
  for(int iParam=0; iParam<Nparams; iParam++){
    char fileName[128], viewName[128];
    sprintf(fileName, "output/info_param%i.msh", iParam);
    sprintf(viewName, "info_param%i", iParam);
    
    FILE* posFile = NULL;
    posFile = fopen(fileName, "w");
    assert(posFile != NULL);
    fprintf(posFile, "$MeshFormat\n");
    fprintf(posFile, "2.1 0 8\n");
    fprintf(posFile, "$EndMeshFormat\n");
    
    // write element node data
    fprintf(posFile, "$ElementNodeData\n");
    fprintf(posFile, "%i\n", 2);
    fprintf(posFile, "\"%s\"\n", viewName); // name of the view
    fprintf(posFile, "%i\n", 0);
    fprintf(posFile, "%i\n", 1);
    fprintf(posFile, "%i\n", 0);
    fprintf(posFile, "%i\n", 3);
    fprintf(posFile, "%i\n", 0);
    fprintf(posFile, "%i\n", 1);  // ("numComp")
    fprintf(posFile, "%i\n", K);  // total number of elementNodeData in this file
    for(int k=0; k<K; k++){
      fprintf(posFile, "%i %i", EMsh[k], NVertTet);
      for(int v=0; v<NVertTet; v++)
        fprintf(posFile, " %g", data[k*Nparams + iParam]);
      fprintf(posFile, "\n");
    }
    fprintf(posFile, "$EndElementNodeData\n");
    fclose(posFile);
  }
}


// ======================================================================
// Export discrete solution in gmsh files
//   Input: Q, K, N, Np, Nfields, _r, _s, _t, timeStep, time
//   Output: -
// ======================================================================

void exportSolGmsh(double *data, int K, int N, int Np, int Nfields, double* _r, double* _s, double* _t, int *EMsh, int timeStep, double time){
  
  for(int iField=0; iField<Nfields; iField++){
    char fileName[128], viewName[128];
    sprintf(fileName, "output/sol_field%i_%i.msh", iField, timeStep);
    sprintf(viewName, "sol_field%i", iField);
    
    FILE* posFile = NULL;
    posFile = fopen(fileName, "w");
    assert(posFile != NULL);
    fprintf(posFile, "$MeshFormat\n");
    fprintf(posFile, "2.1 0 8\n");
    fprintf(posFile, "$EndMeshFormat\n");
    
    // build matrices with powers of monomials & interpolation coefficients
    int* monom = malloc(Np * 3 * sizeof(int));
    double* vdm = malloc(Np*Np * sizeof(double));
    for(int i=0, n=0; i<=N; i++)
      for(int j=0; j<=N; j++)
        for(int k=0; k<=N; k++)
          if(i+j+k <= N){
            monom[3*n+0] = i;
            monom[3*n+1] = j;
            monom[3*n+2] = k;
            n++;
          }
    for(int m=0; m<Np; m++)
      for(int n=0; n<Np; n++)
        vdm[n*Np+m] = pow((_r[n]+1)/2.,monom[3*m+0])
        * pow((_s[n]+1)/2.,monom[3*m+1])
        * pow((_t[n]+1)/2.,monom[3*m+2]);
    
    double* coeff = malloc(Np*Np * sizeof(double));
    inverseMat(Np, vdm, coeff);
    
    // write the interpolation scheme
    fprintf(posFile, "$InterpolationScheme\n");
    fprintf(posFile, "\"MyInterpScheme\"\n");
    fprintf(posFile, "%i\n", 1);
    fprintf(posFile, "%i %i\n", 5, 2);   // 5 is for TET -- 2 is coded as-is in gmsh
    fprintf(posFile, "%i %i\n", Np, Np); // size of matrix 'coeff'
    for(int m=0; m<Np; m++){
      for(int n=0; n<Np; n++)
        fprintf(posFile, "%g ", coeff[n*Np+m]);
      fprintf(posFile, "\n");
    }
    fprintf(posFile, "%i %i\n", Np, 3);  // size of matrix 'monom'
    for(int n=0; n<Np; n++){
      for(int d=0; d<3; d++)
        fprintf(posFile, "%i ", monom[3*n+d]);
      fprintf(posFile, "\n");
    }
    fprintf(posFile, "$EndInterpolationScheme\n");
    
    // write element node data
    fprintf(posFile, "$ElementNodeData\n");
    fprintf(posFile, "%i\n", 2);
    fprintf(posFile, "\"%s\"\n", viewName); // name of the view
    fprintf(posFile, "\"MyInterpScheme\"\n");
    fprintf(posFile, "%i\n", 1);
    fprintf(posFile, "%g\n", time);
    fprintf(posFile, "%i\n", 3);
    fprintf(posFile, "%i\n", timeStep);
    fprintf(posFile, "%i\n", 1);  // ("numComp")
    fprintf(posFile, "%i\n", K);  // total number of elementNodeData in this file
    for(int k=0; k<K; k++){
      fprintf(posFile, "%i %i", EMsh[k], Np);
      for(int v=0; v<Np; v++)
        fprintf(posFile, " %g", data[Nfields*Np*k + Nfields*v + iField]);
      fprintf(posFile, "\n");
    }
    fprintf(posFile, "$EndElementNodeData\n");
    fclose(posFile);

    free(monom);
    free(vdm);
    free(coeff);
  }
  
  double newMax = -1000.;
  for(int k=0; k<K; k++)
    for(int v=0; v<Np; v++)
      for(int iField=0; iField<Nfields; iField++)
        newMax = fmax(newMax, data[k*Np*Nfields + v*Nfields + iField]);
  
  printf("  fields exported (max value: %f at time %f and iter %i)\n", newMax, time, timeStep);
}
