#include "tools.h"

int factorial(int n){
  if(n==0) return 1;
  else return n*factorial(n-1);
}

void inverseMat(int N, double *A, double *B){
  
  double* Acopy = malloc(N*N * sizeof(double));
  for(int n=0; n<N*N; n++)
    Acopy[n] = A[n];
  
  for(int n=0; n<N*N; n++)
    B[n] = 0.;
  for(int n=0; n<N; n++)
    B[n*N+n] = 1.;
  
  int INFO;
  int* PIV = malloc(N * sizeof(int));
  
  // Solve A*X = B, X stored in B at the end
  dgesv_( &N, &N, Acopy, &N, PIV, B, &N, &INFO );
  
  free(Acopy);
  free(PIV);
  return;
}


// ======================================================================
// Function [P] = jacobiP(x,alpha,beta,p)
// Purpose: Evaluate Jacobi Polynomial of type (alpha,beta) > -1 (alpha+beta <> -1) at points x for order N and returns P[1:length(xp))]
// Note   : They are normalized to be orthonormal.
// ======================================================================

double jacobiP(double xp, double alpha, double beta, int p){
  
  double *PL = malloc((p+1)*sizeof(double));
  
  // initial values P_0(x) and P_1(x)
  double gamma0 = pow(2,(alpha+beta+1)) / (alpha+beta+1) * factorial(alpha) * factorial(beta)/factorial(alpha+beta);
  PL[0] = 1.0/sqrt(gamma0);
  if (p==0) return PL[0];
  
  double gamma1 = (alpha+1)*(beta+1) / (alpha+beta+3)*gamma0;
  PL[1] = ((alpha+beta+2)*xp/2 + (alpha-beta)/2)/sqrt(gamma1);
  if (p==1) return PL[1];
  
  // repeat value in recurrence.
  double aold = 2/(2+alpha+beta)*sqrt((alpha+1)*(beta+1)/(alpha+beta+3));
  
  // forward recurrence using the symmetry of the recurrence.
  for(int i=1; i<=p-1; ++i){
    double h1 = 2*i+alpha+beta;
    double anew = 2/(h1+2)*sqrt( (i+1)*(i+1+alpha+beta)*(i+1+alpha)*(i+1+beta)/(h1+1)/(h1+3));
    double bnew = -(alpha*alpha-beta*beta)/h1/(h1+2);
    PL[i+1] = 1./anew*( -aold*PL[i-1] + (xp-bnew)*PL[i]);
    aold =anew;
  }
  
  double out = PL[p];
  free(PL);
  return out;
}


// ======================================================================
// Function [dP] = gradJacobiP(z, alpha, beta, N);
// Purpose: Evaluate the derivative of the orthonormal Jacobi polynomial of type (alpha,beta)>-1,
//          at points x for order N and returns dP[1:length(xp))]
// ======================================================================

double gradJacobiP(double xout, double alpha, double beta, int p){
  if(p == 0)
    return 0.;
  else
    return sqrt(p*(p+alpha+beta+1)) * jacobiP(xout,alpha+1,beta+1, p-1);
}


// ======================================================================
// Function [dmodedr, dmodeds, dmodedt] = gradSimplexP(a,b,c,id,jd,kd)
// Purpose: Return the derivatives of the modal basis (id,jd,kd) on the simplex at (a,b,c).
// ======================================================================

void gradSimplexP(double a, double b, double c, int id, int jd, int kd, double *dmodedrOut, double *dmodedsOut, double *dmodedtOut){
  
  double  fa =     jacobiP(a, 0, 0, id);
  double dfa = gradJacobiP(a, 0, 0, id);
  double  gb =     jacobiP(b, 2*id+1,0, jd);
  double dgb = gradJacobiP(b, 2*id+1,0, jd);
  double  hc =     jacobiP(c, 2*(id+jd)+2,0, kd);
  double dhc = gradJacobiP(c, 2*(id+jd)+2,0, kd);
  
  // r-derivative
  double dmodedr = dfa*gb*hc;
  if(id>0)
    dmodedr = dmodedr*pow(0.5*(1-b),id-1);
  if(id+jd>0)
    dmodedr = dmodedr*pow(0.5*(1-c),id+jd-1);
  
  // s-derivative
  double dmodeds = 0.5*(1+a)*dmodedr;
  double tmp = dgb*pow(0.5*(1-b),id);
  if(id>0)
    tmp = tmp+(-0.5*id)*gb*pow(0.5*(1-b),id-1);
  if(id+jd>0)
    tmp = tmp*pow(0.5*(1-c),id+jd-1);
  tmp = fa*tmp*hc;
  dmodeds += tmp;
  
  // t-derivative
  double dmodedt = 0.5*(1+a)*dmodedr+0.5*(1+b)*tmp;
  tmp = dhc*pow(0.5*(1-c),id+jd);
  if(id+jd>0)
    tmp = tmp-0.5*(id+jd)*hc*pow(0.5*(1-c),id+jd-1);
  tmp = fa*gb*tmp;
  tmp = tmp*pow(0.5*(1-b),id);
  dmodedt +=tmp;
  
  // normalize
  *dmodedrOut = pow(2,2*id+jd+1.5)*dmodedr;
  *dmodedsOut = pow(2,2*id+jd+1.5)*dmodeds;
  *dmodedtOut = pow(2,2*id+jd+1.5)*dmodedt;
}


// ======================================================================
// Function [Vout] = vandermonde2D(r,s,Vout)
// Purpose : Initialize the gradient of the modal basis (i,j) at (r,s) at order p
// ======================================================================

void vandermonde2D(int N, int Nfp, double *r, double *s, double *Vout){
  
  // find tensor-product coordinates
  double *a = malloc(Nfp*sizeof(double));
  double *b = malloc(Nfp*sizeof(double));
  for(int n=0; n<Nfp; ++n){
    a[n] = -1;
    if(fabs(s[n]-1)>1e-5)
      a[n] = 2*(1+r[n])/(1-s[n])-1;
    b[n] = s[n];
  }
  
  // initialize matrix
  int sk=0;
  for(int i=0; i<=N; ++i){
    for(int j=0; j<=N-i; ++j){
      for(int n=0; n<Nfp; ++n){
        double h1 = jacobiP(a[n],0,0,i);
        double h2 = jacobiP(b[n],2*i+1,0,j);
        Vout[sk*Nfp + n] = sqrt(2.) * h1 * h2 * pow(1-b[n],i);
      }
      sk++;
    }
  }

  free(a);
  free(b);
}


// ======================================================================
// Function [Vout] = vandermonde3D(r,s,t,Vout)
// Purpose : Initialize the gradient of the modal basis (i,j,k) at (r,s,t) at order p
// ======================================================================

void vandermonde3D(int N, int Np, double *r, double *s, double *t, double *Vout){
  
  // find tensor-product coordinates
  double *a = malloc(Np*sizeof(double));
  double *b = malloc(Np*sizeof(double));
  double *c = malloc(Np*sizeof(double));
  for(int n=0; n<Np; ++n){
    a[n] = -1;
    b[n] = -1;
    if(fabs(s[n]+t[n])>1e-5)
      a[n] = 2*(1+r[n])/(-s[n]-t[n])-1;
    if(fabs(t[n]-1)>1e-5)
      b[n] = 2*(1+s[n])/(1-t[n])-1;
    c[n] = t[n];
  }
  
  // initialize matrix
  int sk=0;
  for(int i=0; i<=N; ++i){
    for(int j=0; j<=N-i; ++j){
      for(int k=0; k<=N-i-j; ++k){
        for(int n=0; n<Np; ++n){
          double h1 = jacobiP(a[n],0,0,i);
          double h2 = jacobiP(b[n],2*i+1,0,j);
          double h3 = jacobiP(c[n],2*(i+j)+2,0,k);
          Vout[sk*Np + n] = 2*sqrt(2.) * h1*h2*h3 * pow(1-b[n],i) * pow(1-c[n],i+j);
        }
        sk++;
      }
    }
  }

  free(a);
  free(b);
  free(c);
}


// ======================================================================
// Function [Vr,Vs,Vt] = gradVandermonde3D(r,s,t)
// Purpose : Initialize the gradient of the modal basis (i,j,k) at (r,s,t) at order N
// ======================================================================

void gradVandermonde3D(int N, int Np, double *r, double *s, double *t, double *Vr, double *Vs, double *Vt){
  
  // find tensor-product coordinates
  double *a = malloc(Np*sizeof(double));
  double *b = malloc(Np*sizeof(double));
  double *c = malloc(Np*sizeof(double));
  for(int n=0; n<Np; ++n){
    a[n] = -1;
    b[n] = -1;
    if(fabs(s[n]+t[n])>1e-5)
      a[n] = 2*(1+r[n])/(-s[n]-t[n])-1;
    if(fabs(t[n]-1)>1e-5)
      b[n] = 2*(1+s[n])/(1-t[n])-1;
    c[n] = t[n];
  }
  
  // initialize matrices
  int sk=0;
  for(int i=0; i<=N; ++i){
    for(int j=0; j<=N-i; ++j){
      for(int k=0; k<=N-i-j; ++k){
        for(int n=0; n<Np; ++n)
          gradSimplexP(a[n], b[n], c[n], i,j,k, &Vr[sk*Np + n], &Vs[sk*Np + n], &Vt[sk*Np + n]);
        sk++;
      }
    }
  }

  free(a);
  free(b);
  free(c);
}
