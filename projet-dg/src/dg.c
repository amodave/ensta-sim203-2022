#include "dg.h"

// ======================================================================
// Build connectivity matrices "elementToElement" and "elementToFace"
//   Input: K, NFaceVertTet, NFacesTet, EToV
//   Output: _EToE, _EToF
// ======================================================================

void buildEToE(int K, int *_EToV, int **_EToE, int **_EToF){
  printf("CALL buildEToE()\n");
  
  // build a face-to-vertex connectivity array "FToV"
  int* FToV = malloc((NFacesTet*K) * (NFaceVertTet+2) * sizeof(int));
  for(int i=0; i<(NFacesTet*K) * (NFaceVertTet+2); i++)
    FToV[i] = 0;
  
  int VfToVe[4][3] = {{0,1,2}, {0,1,3}, {1,2,3}, {0,2,3}};
  
  int cnt = 0;
  for(int k=0; k<K; k++){
    for(int f=0; f<NFacesTet; f++){
      
      // raw sort of vertex numbers on this face
      int* ns = malloc(NFaceVertTet * sizeof(int));
      for(int i=0; i<NFaceVertTet; ++i){
        ns[i] = _EToV[k*NVertTet + VfToVe[f][i]];
      }
      for(int i=0; i<NFaceVertTet; ++i){
        for(int j=i+1; j<NFaceVertTet; ++j){
          if(ns[j]<ns[i]){
            int tmp = ns[i];
            ns[i] = ns[j];
            ns[j] = tmp;
          }
        }
      }
      
      FToV[cnt*(NFaceVertTet+2) + 0] = k;  // element number
      FToV[cnt*(NFaceVertTet+2) + 1] = f;  // face number
      for(int i=0; i<NFaceVertTet; ++i){
        FToV[cnt*(NFaceVertTet+2) + 2+i] = ns[i];  // vertex numbers
      }
      ++cnt;

      free(ns);
    }
  }
  
  // sort by 3rd row (forgot column major coNVertTetention)
  qsort(FToV, (NFacesTet*K), (NFaceVertTet+2)*sizeof(int), compareEntries);
  
  // build 'EToE' and 'EToF' connectivity arrays (1-indexed)
  int *EToE = malloc(K*NFacesTet*sizeof(int));
  int *EToF = malloc(K*NFacesTet*sizeof(int));
  for(int i=0; i<K*NFacesTet; i++){
    EToE[i] = -1;
    EToF[i] = -1;
  }
  
  // find neighbors
  for(cnt=0; cnt<K*NFacesTet-1; ++cnt){
    int neighbor = 1;
    for(int vert=0; vert<NFaceVertTet; vert++){
      if(FToV[cnt*(NFaceVertTet+2) + 2+vert] != FToV[(cnt+1)*(NFaceVertTet+2) + 2+vert])
        neighbor = 0;
    }
    if(neighbor == 1){
      int k1 = FToV[cnt*(NFaceVertTet+2) + 0];
      int f1 = FToV[cnt*(NFaceVertTet+2) + 1];
      int k2 = FToV[(cnt+1)*(NFaceVertTet+2) + 0];
      int f2 = FToV[(cnt+1)*(NFaceVertTet+2) + 1];
      EToE[k1*NFacesTet + f1] = k2;
      EToE[k2*NFacesTet + f2] = k1;
      EToF[k1*NFacesTet + f1] = f2;
      EToF[k2*NFacesTet + f2] = f1;
    }
  }
  
  for(int k=0; k<K; ++k){
    for(int f=0; f<NFacesTet; ++f){
      if(EToE[k*NFacesTet + f] == -1){
        EToE[k*NFacesTet + f] = k;
        EToF[k*NFacesTet + f] = f;
      }
    }
  }
  
  *_EToE = EToE;
  *_EToF = EToF;

  free(FToV);
}

int compareEntries(const void *a, const void *b){
  const int *aI = a;
  const int *bI = b;
  
  int a1 = aI[2], a2 = aI[3], a3 = aI[4];
  int b1 = bI[2], b2 = bI[3], b3 = bI[4];
  
  if(b1>a1) return -1;
  if(a1>b1) return  1;
  
  if(b2>a2) return -1;
  if(a2>b2) return  1;
  
  if(b3>a3) return -1;
  if(a3>b3) return  1;
  
  return 0;
}


// ======================================================================
// Build connectivity matrix "faceNode-to-faceNode"
//   Input: K, NFacesTet, Nfp, _Fmask, _EToE, _EToF, _x, _y, _z, _TOL
//   Output: _mapP
// ======================================================================

void buildNfToNf(int K, int Nfp, int Np, int *_Fmask, int *_EToE, int *_EToF, double *_x, double *_y, double *_z, double _TOL, int **_mapP){
  printf("CALL buildNfToNf()\n");
  
  int* mapP = malloc(K * Nfp*NFacesTet*sizeof(int));
  
  for(int k1=0; k1<K; ++k1){
    for(int f1=0; f1<NFacesTet; ++f1){
      for(int nf1=0; nf1<Nfp; ++nf1){
        
        // local index of face node 'n1' (on face 'f1')
        // in the list of nodes of the ref elem
        int n1 = _Fmask[f1*Nfp + nf1];
        
        // global index of face node 'n1' (on face 'f1' of local elem 'k1')
        // in the list of all face nodes of the process
        int n1GlobIndex = nf1 + f1*Nfp + k1*NFacesTet*Nfp;
        mapP[n1GlobIndex] = -1;
        
        // global index of neighbor element
        // and local index of neighbor face
        int k2 = _EToE[k1*NFacesTet + f1];
        int f2 = _EToF[k1*NFacesTet + f1];
        
        if(k2!=k1){  // if there is a neighbor
          for(int nf2=0; nf2<Nfp; ++nf2){
            
            // local index of the face node 'nf2' on face 'f2'
            // in the list of nodes of the ref elem
            int n2 = _Fmask[f2*Nfp + nf2];
            
            double dist =
            sqrt(pow(_x[k1*Np+n1]-_x[k2*Np+n2],2) +  // distance based on global coordinates
                 pow(_y[k1*Np+n1]-_y[k2*Np+n2],2) +  // of face node on both sides
                 pow(_z[k1*Np+n1]-_z[k2*Np+n2],2));
            
            // possible connection
            if(dist<_TOL){
              
              // global index of face node 'n2' (on face 'f2' of neighbor elem 'k2')
              // in the list of all face nodes of the process [1-indexing]
              mapP[n1GlobIndex] = n2;
              
            }
          }
          if( mapP[n1GlobIndex]<0 ){
            printf("There is a problem ...\n");
            exit(1);
          }
        }
      }
    }
  }
  
  *_mapP = mapP;
}


// ======================================================================
// Build geometric factors (volume and surface)
//   Input: K, NFacesTet, NVertTet, EToV, _VX, _VY, _VZ
//   Output: _rstxyz, _Fscale, _nx, _ny, _nz
// ======================================================================

void buildGeomFactors(int K, int *EToV, double *_VX, double *_VY, double *_VZ, double **_rstxyz, double **_Fscale, double **_nx, double **_ny, double **_nz){
  printf("CALL buildGeomFactors()\n");
  
  double *rstxyz = malloc(K*9*sizeof(double));           // ... for volume terms  [K,9]
  double *Fscale = malloc(K*NFacesTet*sizeof(double));   // ... for surface terms [K,NFacesTet]
  double *nx = malloc(K*NFacesTet*sizeof(double));       // x-component of normal to the face [K,NFacesTet]
  double *ny = malloc(K*NFacesTet*sizeof(double));       // y-component of normal to the face [K,NFacesTet]
  double *nz = malloc(K*NFacesTet*sizeof(double));       // z-component of normal to the face [K,NFacesTet]
  
  for(int k=0; k<K; ++k){
    
    int vGlo1 = EToV[k*NVertTet + 0];
    int vGlo2 = EToV[k*NVertTet + 1];
    int vGlo3 = EToV[k*NVertTet + 2];
    int vGlo4 = EToV[k*NVertTet + 3];
    double x1=_VX[vGlo1], y1=_VY[vGlo1], z1=_VZ[vGlo1];
    double x2=_VX[vGlo2], y2=_VY[vGlo2], z2=_VZ[vGlo2];
    double x3=_VX[vGlo3], y3=_VY[vGlo3], z3=_VZ[vGlo3];
    double x4=_VX[vGlo4], y4=_VY[vGlo4], z4=_VZ[vGlo4];
    
    double xrk=0.5*(x2-x1), yrk=0.5*(y2-y1), zrk=0.5*(z2-z1);
    double xsk=0.5*(x3-x1), ysk=0.5*(y3-y1), zsk=0.5*(z3-z1);
    double xtk=0.5*(x4-x1), ytk=0.5*(y4-y1), ztk=0.5*(z4-z1);
    
    double Jk = xrk*(ysk*ztk-zsk*ytk) - yrk*(xsk*ztk-zsk*xtk) + zrk*(xsk*ytk-ysk*xtk);
    
    double rx =  (ysk*ztk - zsk*ytk)/Jk;
    double ry = -(xsk*ztk - zsk*xtk)/Jk;
    double rz =  (xsk*ytk - ysk*xtk)/Jk;
    double sx = -(yrk*ztk - zrk*ytk)/Jk;
    double sy =  (xrk*ztk - zrk*xtk)/Jk;
    double sz = -(xrk*ytk - yrk*xtk)/Jk;
    double tx =  (yrk*zsk - zrk*ysk)/Jk;
    double ty = -(xrk*zsk - zrk*xsk)/Jk;
    double tz =  (xrk*ysk - yrk*xsk)/Jk;
    
    rstxyz[k*9+0] = rx;
    rstxyz[k*9+1] = ry;
    rstxyz[k*9+2] = rz;
    rstxyz[k*9+3] = sx;
    rstxyz[k*9+4] = sy;
    rstxyz[k*9+5] = sz;
    rstxyz[k*9+6] = tx;
    rstxyz[k*9+7] = ty;
    rstxyz[k*9+8] = tz;
    
    double nxLoc[NFacesTet] = {-tx, -sx, rx+sx+tx, -rx};
    double nyLoc[NFacesTet] = {-ty, -sy, ry+sy+ty, -ry};
    double nzLoc[NFacesTet] = {-tz, -sz, rz+sz+tz, -rz};
    
    // normalise and store
    for(int f=0; f<NFacesTet; ++f){
      double sJ = sqrt(nxLoc[f]*nxLoc[f] + nyLoc[f]*nyLoc[f] + nzLoc[f]*nzLoc[f]);
      Fscale[k*NFacesTet + f] = sJ;
      nx[k*NFacesTet + f] = nxLoc[f]/sJ;
      ny[k*NFacesTet + f] = nyLoc[f]/sJ;
      nz[k*NFacesTet + f] = nzLoc[f]/sJ;
    }
  }
  
  *_rstxyz = rstxyz;
  *_Fscale = Fscale;
  *_nx = nx;
  *_ny = ny;
  *_nz = nz;
}


// ======================================================================
// Build elemental matrices (volume and surface)
//   Input: NFacesTet, N, Np, Nfp, _r, _s, _t, _Fmask
//   Output: _Drst, _LIFT
// ======================================================================

void buildElemMatrices(int N, int Np, int Nfp, double *_r, double *_s, double *_t, int *_Fmask, double **_Dr, double **_Ds, double **_Dt, double **_LIFT){
  printf("CALL buildElemMatrices()\n");
  
  // Differentiation matrices
  
  double *V          = malloc(Np*Np * sizeof(double));
  double *ViNVertTet = malloc(Np*Np * sizeof(double));
  double *Vr         = malloc(Np*Np * sizeof(double));
  double *Vs         = malloc(Np*Np * sizeof(double));
  double *Vt         = malloc(Np*Np * sizeof(double));
  double *Dr         = malloc(Np*Np * sizeof(double));  // r-derivative matrix
  double *Ds         = malloc(Np*Np * sizeof(double));  // s-derivative matrix
  double *Dt         = malloc(Np*Np * sizeof(double));  // t-derivative matrix

  vandermonde3D(N, Np, _r,_s,_t, V);
  inverseMat(Np, V, ViNVertTet);
  gradVandermonde3D(N, Np, _r,_s,_t, Vr,Vs,Vt);
  
  for(int m=0; m<Np; ++m){
    for(int n=0; n<Np; ++n){
      Dr[n*Np + m] = 0.;
      Ds[n*Np + m] = 0.;
      Dt[n*Np + m] = 0.;
      for(int k=0; k<Np; ++k){
        Dr[n*Np + m] += Vr[k*Np + m] * ViNVertTet[n*Np + k];
        Ds[n*Np + m] += Vs[k*Np + m] * ViNVertTet[n*Np + k];
        Dt[n*Np + m] += Vt[k*Np + m] * ViNVertTet[n*Np + k];
      }
    }
  }
  
  *_Dr = Dr;
  *_Ds = Ds;
  *_Dt = Dt;
  
  // Lift matrix
  
  double *Emat = malloc(NFacesTet*Nfp * Np * sizeof(double));
  double *LIFT = malloc(NFacesTet*Nfp * Np * sizeof(double));

  for(int i=0; i<NFacesTet*Nfp; ++i)
    for(int j=0; j<Np; ++j)
      Emat[Np*i+j] = 0.;
  
  for(int f=0; f<NFacesTet; ++f){
    
    double *faceR                 = malloc(Nfp * sizeof(double));
    double *faceS                 = malloc(Nfp * sizeof(double));
    double *VFace                 = malloc(Nfp*Nfp * sizeof(double));
    double *massFaceINVertTeterse = malloc(Nfp*Nfp * sizeof(double));
    double *massFace              = malloc(Nfp*Nfp * sizeof(double));

    for(int nf=0; nf<Nfp; ++nf){
      int n = _Fmask[f*Nfp + nf];
      switch(f){
        case 0: faceR[nf] = _r[n]; faceS[nf] = _s[n]; break;
        case 1: faceR[nf] = _r[n]; faceS[nf] = _t[n]; break;
        case 2: faceR[nf] = _s[n]; faceS[nf] = _t[n]; break;
        case 3: faceR[nf] = _s[n]; faceS[nf] = _t[n]; break;
      }
    }
    
    vandermonde2D(N, Nfp, faceR, faceS, VFace);
    
    for(int i=0; i<Nfp; i++){
      for(int j=0; j<Nfp; j++){
        massFaceINVertTeterse[i*Nfp + j] = 0.;
        for(int k=0; k<Nfp; k++)
          massFaceINVertTeterse[i*Nfp + j] += VFace[k*Nfp + i] * VFace[k*Nfp + j];
      }
    }
    
    inverseMat(Nfp, massFaceINVertTeterse, massFace);
    
    for(int nf=0; nf<Nfp; ++nf){
      for(int mf=0; mf<Nfp; ++mf){
        int idr = _Fmask[f*Nfp + nf];
        int idc = mf + f*Nfp;
        Emat[idc*Np+idr] += massFace[mf*Nfp + nf];
      }
    }

    free(faceR);
    free(faceS);
    free(VFace);
    free(massFace);
    free(massFaceINVertTeterse);
  }
  
  for(int m=0; m<Nfp*NFacesTet; m++){
    double *tmp = malloc(Np*sizeof(double));
    for(int n=0; n<Np; n++){
      tmp[n] = 0.;
      for(int k=0; k<Np; k++)
        tmp[n] += Emat[m*Np+k]*V[n*Np+k];
    }
    for(int n=0; n<Np; n++){
      LIFT[n*NFacesTet*Nfp + m] = 0.;
      for(int l=0; l<Np; l++)
        LIFT[n*NFacesTet*Nfp + m] += tmp[l]*V[l*Np+n];
    }
    free(tmp);
  }
  
  *_LIFT = LIFT;

  free(V);
  free(ViNVertTet);
  free(Vr);
  free(Vs);
  free(Vt);
  free(Emat);
}
