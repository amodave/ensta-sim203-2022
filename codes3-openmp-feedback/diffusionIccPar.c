// Compilation:
//   icc -O0 -std=c99 -mkl diffusionIccPar.c
//   icc -O1 -std=c99 -mkl diffusionIccPar.c
//   icc -O2 -std=c99 -mkl diffusionIccPar.c
//   icc -O3 -std=c99 -mkl diffusionIccPar.c
//   icc -O3 -std=c99 -mkl -qopenmp -qopt-report=1 -qopt-report-phase=vec,par,openmp -qopt-report-annotate=html diffusionIccPar.c
// Execution:
//   ./a.out 'version' 'T' 'N'
//   ./a.out 4 1000 1024; ./a.out 7 1000 1024; ./a.out 8 1000 1024; ./a.out 9 1000 1024;

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mkl.h>

void switchPointer(double** x1, double** x2);
void implementation4(int T, int N, double* C, double* Cnew);
void implementation7(int T, int N, double* C, double* Cnew);
void implementation8(int T, int N, double* C, double* Cnew);
void implementation9(int T, int N, double* C, double* Cnew);

int main(int argc, char* argv[])
{

  // Parameters
  int version, T, N;
  if(argc == 4){
    version = atoi(argv[1]);
    T = atoi(argv[2]);
    N = atoi(argv[3]);
  }
  else{
    printf("3 arguments nécessaires : version, T, N\n");
    return 1;
  }

  // Initialization of arrays
  double* C    = malloc(N*N*sizeof(double));
  double* Cnew = malloc(N*N*sizeof(double));
  for(int i=0; i<N; i++){
    for(int j=0; j<N; j++){
      double x = ((double)i)/(N-1.);
      double y = ((double)j)/(N-1.);
      C[i*N+j] = exp(-((x-0.5)*(x-0.5)+(y-0.5)*(y-0.5))/0.01);
    }
  }

  // Start CHRONO
  const double timeBegin = dsecnd();

  if(version == 4) implementation4(T, N, C, Cnew);
  if(version == 7) implementation7(T, N, C, Cnew);
  if(version == 8) implementation8(T, N, C, Cnew);
  if(version == 9) implementation9(T, N, C, Cnew);

  // End CHRONO
  const double timeEnd = dsecnd();
  double timeTotal = timeEnd-timeBegin;
  printf("%i %f %f\n", version, timeTotal, C[N*N/2 + N/2]);

  return 0;
}

// Switch of pointers
void switchPointer(double** x1, double** x2){
  double* tmp;
  tmp = *x1;
  *x1 = *x2;
  *x2 = tmp;
}

// Implementation 4
void implementation4(int T, int N, double* C, double* Cnew){
  double dx = 1./(double)(N-1);
  double dt = 0.2*dx*dx;
  double coef1 = dt/(dx*dx);
  double coef2 = 1 - 4*coef1;

  for(int n=0; n<T; n++){
    for(int i=1; i<(N-1); i++)
#pragma ivdep
      for(int j=1; j<(N-1); j++)
        Cnew[N*i+j]
          = coef2 * C[N*i+j]
          + coef1 * ( C[N*(i+1)+j] + C[N*(i-1)+j] + C[N*i+(j+1)] + C[N*i+(j-1)] );
    switchPointer(&C,&Cnew);
  }
}

// Implementation 7
void implementation7(int T, int N, double* C, double* Cnew){
  double dx = 1./(double)(N-1);
  double dt = 0.2*dx*dx;
  double coef1 = dt/(dx*dx);
  double coef2 = 1 - 4*coef1;

#pragma omp parallel for
  for(int n=0; n<T; n++){
    for(int i=1; i<(N-1); i++)
#pragma ivdep
      for(int j=1; j<(N-1); j++)
        Cnew[N*i+j]
          = coef2 * C[N*i+j]
          + coef1 * ( C[N*(i+1)+j] + C[N*(i-1)+j] + C[N*i+(j+1)] + C[N*i+(j-1)] );
    switchPointer(&C,&Cnew);
  }
}

// Implementation 8
void implementation8(int T, int N, double* C, double* Cnew){
  double dx = 1./(double)(N-1);
  double dt = 0.2*dx*dx;
  double coef1 = dt/(dx*dx);
  double coef2 = 1 - 4*coef1;

  for(int n=0; n<T; n++){
#pragma omp parallel for
    for(int i=1; i<(N-1); i++)
#pragma ivdep
      for(int j=1; j<(N-1); j++)
        Cnew[N*i+j]
          = coef2 * C[N*i+j]
          + coef1 * ( C[N*(i+1)+j] + C[N*(i-1)+j] + C[N*i+(j+1)] + C[N*i+(j-1)] );
    switchPointer(&C,&Cnew);
  }
}

// Implementation 9
void implementation9(int T, int N, double* C, double* Cnew){
  double dx = 1./(double)(N-1);
  double dt = 0.2*dx*dx;
  double coef1 = dt/(dx*dx);
  double coef2 = 1 - 4*coef1;

  for(int n=0; n<T; n++){
		for(int i=1; i<(N-1); i++)
#pragma omp parallel for
#pragma ivdep
      for(int j=1; j<(N-1); j++)
        Cnew[N*i+j]
          = coef2 * C[N*i+j]
          + coef1 * ( C[N*(i+1)+j] + C[N*(i-1)+j] + C[N*i+(j+1)] + C[N*i+(j-1)] );
    switchPointer(&C,&Cnew);
  }
}
