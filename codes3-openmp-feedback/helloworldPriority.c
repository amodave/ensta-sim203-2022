// Compilation:
//   icc -qopenmp helloworldPriority.c
// Execution:
//   export OMP_NUM_THREADS=4
//   ./a.out

#include <stdio.h>
#include <omp.h>

int main(){
//  omp_set_num_threads(8);
#pragma omp parallel
  printf("Hello world!\n");
  return 0;
}
