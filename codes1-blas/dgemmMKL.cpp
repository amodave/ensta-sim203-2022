// Compilation
//   icpc -mkl dgemmMKL.c 
// Execution:
//   ./a.out

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <mkl.h>

#ifndef M_PI
  #define M_PI 3.14159265359
#endif

void initArray(double *array, int size) {
  for(int i=0; i<size; i++)
    array[i] = sin(M_PI*(1./((double)i+1)));
}

int main()
{

  // parameter of the problem
  int n = 1024;

  // parameters for BLAS
  enum CBLAS_LAYOUT Order = CblasRowMajor;
  enum CBLAS_TRANSPOSE TransA = CblasTrans;
  enum CBLAS_TRANSPOSE TransB = CblasNoTrans;
  int M = n;
  int N = n;
  int K = n;
  int lda = n;
  int ldb = n;
  int ldc = n;

  // scalar and matrices
  double alpha = 1.;
  double beta  = 1.;
  double *A = (double*)malloc(n*n * sizeof(double));
  double *B = (double*)malloc(n*n * sizeof(double));
  double *C = (double*)malloc(n*n * sizeof(double));
  initArray(A, n*n);
  initArray(B, n*n);
  initArray(C, n*n);

  // timing: start
  const clock_t            timeBegin1 = clock();
  const float              timeBegin2 = second();
  const double             timeBegin3 = dsecnd();
  unsigned MKL_INT64 timeBegin4;
  mkl_get_cpu_clocks(&timeBegin4);

  cblas_dgemm(Order, TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc);

  // timing: end
  const clock_t            timeEnd1 = clock();
  const float              timeEnd2 = second();
  const double             timeEnd3 = dsecnd();
  unsigned MKL_INT64 timeEnd4;
  mkl_get_cpu_clocks(&timeEnd4);

  double timeTotal1 = (double)(timeEnd1-timeBegin1)/CLOCKS_PER_SEC;
  double timeTotal2 = (double)(timeEnd2-timeBegin2);
  double timeTotal3 = (double)(timeEnd3-timeBegin3);
  double timeTotal4 = (double)(timeEnd4-timeBegin4)/mkl_get_clocks_frequency()/1e9;
  printf("Runtimes: %f sec, %f sec, %f sec and %f sec\n", timeTotal1, timeTotal2, timeTotal3, timeTotal4);

  free(A);
  free(B);
  free(C);
  return 0;
}
