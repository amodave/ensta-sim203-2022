// Compilation
//   gcc dgemmLapack.c -lm -lblas -std=c99
// Execution:
//   ./a.out

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <cblas.h>

#ifndef M_PI
  #define M_PI 3.14159265359
#endif

void initArray(double *array, int size) {
  for(int i=0; i<size; i++)
    array[i] = sin(M_PI*(1./((double)i+1)));
}

int main()
{

  // parameter of the problem
  int n = 1024;

  // parameters for BLAS
  int M = n;
  int N = n;
  int K = n;
  int lda = n;
  int ldb = n;
  int ldc = n;

  // scalar and matrices
  double alpha = 1.;
  double beta  = 1.;
  double *A = malloc(n*n * sizeof(double));
  double *B = malloc(n*n * sizeof(double));
  double *C = malloc(n*n * sizeof(double));
  initArray(A, n*n);
  initArray(B, n*n);
  initArray(C, n*n);

  // timing: start
  const clock_t timeBegin = clock();

  cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc);

  // timing: end
  const clock_t timeEnd = clock();
  double timeTotal = (double)(timeEnd-timeBegin)/CLOCKS_PER_SEC;
  printf("Runtime: %f sec\n", timeTotal);

  free(A);
  free(B);
  free(C);
  return 0;
}
